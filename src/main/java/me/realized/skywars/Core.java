package me.realized.skywars;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;

import me.realized.skywars.commands.BukkitExecutor;
import me.realized.skywars.configuration.MyConfig;
import me.realized.skywars.listeners.PlayerListener;
import me.realized.skywars.listeners.ServerListener;
import me.realized.skywars.players.DataManager;
import me.realized.skywars.game.GameManager;
import me.realized.skywars.scoreboard.BoardManager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Core extends JavaPlugin {

    private static Core instance = null;

    private WorldEditPlugin worldedit;
    private MyConfig config;
    private GameManager gameManager;
    private DataManager dataManager;
    private BoardManager boardManager;

    @Override
    public void onEnable() {
        instance = this;

        PluginManager manager = Bukkit.getPluginManager();

        worldedit = (WorldEditPlugin) manager.getPlugin("WorldEdit");

        config = new MyConfig(this);

        dataManager = new DataManager(this);

        gameManager = new GameManager(this);

        if (gameManager.load()) {
            boardManager = new BoardManager(this);

            manager.registerEvents(new PlayerListener(this), this);
            manager.registerEvents(new ServerListener(this), this);
        } else {
            warn("GameManager has failed to load! Plugin will not run properly.");
        }

        getCommand("skywars").setExecutor(new BukkitExecutor());
    }

    public void info(String msg) {
        getLogger().info(msg);
    }

    public void warn(String msg) {
        getLogger().warning(msg);
    }

    public WorldEditPlugin getWorldEdit() {
        return worldedit;
    }

    public MyConfig getConfiguration() {
        return config;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public BoardManager getBoardManager() {
        return boardManager;
    }

    public static Core getInstance() {
        return instance;
    }
}
