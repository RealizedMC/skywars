package me.realized.skywars.commands;

import me.realized.skywars.utilities.StringUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class BukkitExecutor implements CommandExecutor {

    private static final String CMD_PACKAGE = "me.realized.skywars.commands.subcommands.";

    private Map<String, SkyWarsCommand> commands = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(ChatColor.RED + "Usage: /skywars <stats:edit:setup>");
            return true;
        }

        String input = args[0].toLowerCase();
        ClassLoader classLoader = getClass().getClassLoader();
        SkyWarsCommand subCommand;

        if (commands.get(input) != null) {
            subCommand = commands.get(input);
        } else {
            try {
                Class<?> clazz = classLoader.loadClass(CMD_PACKAGE + StringUtil.capitalize(input));
                Constructor<?> constructor = clazz.getConstructor();
                subCommand = (SkyWarsCommand) constructor.newInstance();
                commands.put(input, subCommand);
            } catch (Exception e) {
                sender.sendMessage(ChatColor.RED + "Invalid command.");
                sender.sendMessage(ChatColor.RED + "Usage: /skywars <stats:edit:setup>");
                return true;
            }
        }

        if (!subCommand.isConsoleExecutable() && !(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command cannot be executed from CONSOLE.");
            return true;
        }

        if (args.length < subCommand.length()) {
            sender.sendMessage(ChatColor.RED + subCommand.description());
            sender.sendMessage(ChatColor.RED + "/skywars " + subCommand.usage());
            return true;
        }

        subCommand.run(sender, args);
        return true;
    }
}
