package me.realized.skywars.commands;

import me.realized.skywars.Core;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public abstract class SkyWarsCommand {

    private final String name;
    private final String usage;
    private final String desc;
    private final int length;
    private final boolean console;

    protected transient final Core instance = Core.getInstance();

    protected SkyWarsCommand(String name, String usage, String desc, int length, boolean console) {
        this.name = name;
        this.usage = usage;
        this.desc = desc;
        this.length = length;
        this.console = console;
    }

    public String name() {
        return name;
    }

    public String usage() {
        return usage;
    }

    public String description() {
        return desc;
    }

    public int length() {
        return length;
    }

    public boolean isConsoleExecutable() {
        return console;
    }

    protected void msg(CommandSender sender, String msg) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
    }

    protected abstract void run(CommandSender sender, String[] args);
}
