package me.realized.skywars.commands.subcommands;

import me.realized.skywars.commands.SkyWarsCommand;
import me.realized.skywars.players.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Edit extends SkyWarsCommand {

    public Edit() {
        super("edit", "edit [username] <add:remove:set> <wins:losses:kills:deaths:score> <amount>", "Edit players stats.", 5, true);
    }

    @Override
    protected void run(CommandSender sender, String[] args) {
        Player target = Bukkit.getPlayerExact(args[1]);

        if (target == null) {
            msg(sender, "&cPlayer not found.");
            return;
        }

        GamePlayer gPlayer = instance.getDataManager().getPlayer(target.getUniqueId());

        if (gPlayer == null) {
            msg(sender, "&cPlayer not found.");
            return;
        }

        if (!GamePlayer.EditType.isValue(args[2].toUpperCase())) {
            msg(sender, "&c" + args[2] + " is an invalid operation type. (Possible: add, remove, set");
            return;
        }

        if (!GamePlayer.StatsType.isValue(args[3].toUpperCase())) {
            msg(sender, "&c" + args[3] + " is an invalid operation type. (Possible: wins, losses, kills, deaths, score");
            return;
        }

        GamePlayer.EditType editType = GamePlayer.EditType.valueOf(args[2].toUpperCase());
        GamePlayer.StatsType statsType = GamePlayer.StatsType.valueOf(args[3].toUpperCase());
        int amount;

        try {
            amount = Integer.parseInt(args[4]);
        } catch (NumberFormatException e) {
            msg(sender, "&c" + args[4] + " is an invalid amount. Please type a valid number.");
            return;
        }

        gPlayer.edit(statsType, editType, amount);
        msg(sender, "&7Action executed for &f" + target.getName() + "&7: &e" + args[2].toLowerCase() + " " + amount + " " + args[3].toLowerCase());
    }
}
