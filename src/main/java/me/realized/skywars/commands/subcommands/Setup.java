package me.realized.skywars.commands.subcommands;

import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;
import me.realized.skywars.commands.SkyWarsCommand;
import me.realized.skywars.game.GameMap;
import me.realized.skywars.utilities.LocationUtil;
import me.realized.skywars.utilities.MyBlock;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class Setup extends SkyWarsCommand {

    public Setup() {
        super("setup", "setup <center:box:done>", "Setup the arena.", 2, false);
    }

    @Override
    protected void run(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        String type = args[1].toLowerCase();

        switch (type) {
            case "center":
                instance.getConfig().set("center", LocationUtil.to(player.getLocation()));
                instance.saveConfig();
                player.sendMessage("Center set to your current location.");
                return;
            case "box":
                Selection selection = instance.getWorldEdit().getSelection(player);

                if (selection == null || !(selection instanceof CuboidSelection)) {
                    player.sendMessage("Please select a cuboid.");
                    return;
                }

                CuboidSelection cuboid = (CuboidSelection) selection;
                Location minimum = cuboid.getMinimumPoint();
                Location maximum = cuboid.getMaximumPoint();
                List<MyBlock> blocks = new ArrayList<>();

                for (int x = minimum.getBlockX(); x <= maximum.getBlockX(); x++) {
                    for (int y = minimum.getBlockY(); y <= maximum.getBlockY(); y++) {
                        for (int z = minimum.getBlockZ(); z <= maximum.getBlockZ(); z++) {
                            Block block = player.getWorld().getBlockAt(x, y, z);

                            if (block != null && block.getType() != Material.AIR) {
                                blocks.add(new MyBlock(block));
                            }
                        }
                    }
                }

                if (blocks.isEmpty()) {
                    player.sendMessage("No blocks found in the cuboid selection.");
                    return;
                }

                File file = new File(instance.getDataFolder(), "arena.dat");

                if (file.exists()) {
                    player.sendMessage("File 'arena.dat' Already exists. Please remove it to generate your arena.");
                    return;
                }

                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }

                try (ObjectOutputStream in = new ObjectOutputStream(new FileOutputStream(file))) {
                    in.writeObject(new GameMap(blocks, LocationUtil.toSimpleLocation(minimum), LocationUtil.toSimpleLocation(maximum)));
                } catch (IOException e) {
                    instance.getLogger().log(Level.SEVERE, "An error occured while trying to save the arena!", e);
                }

                player.sendMessage("Arena save complete.");
        }
    }
}
