package me.realized.skywars.commands.subcommands;

import me.realized.skywars.commands.SkyWarsCommand;
import me.realized.skywars.players.GamePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Stats extends SkyWarsCommand {

    public Stats() {
        super("stats", "stats", "Display your stats.", 1, false);
    }

    @Override
    protected void run(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        GamePlayer gPlayer = instance.getDataManager().getPlayer(player.getUniqueId());

        if (gPlayer == null) {
            msg(player, "&c&lYour data is improperly loaded! Please try re-logging.");
            return;
        }

        msg(player, "&eDisplaying stats of &7" + player.getName() + " &e-");
        msg(player, "&eScore: &7" + gPlayer.get(GamePlayer.StatsType.SCORE));
        msg(player, "&eWins: &7" + gPlayer.get(GamePlayer.StatsType.WINS));
        msg(player, "&eLosses: &7" + gPlayer.get(GamePlayer.StatsType.LOSSES));
        msg(player, "&eKills: &7" + gPlayer.get(GamePlayer.StatsType.KILLS));
        msg(player, "&eDeaths: &7" + gPlayer.get(GamePlayer.StatsType.DEATHS));
    }
}
