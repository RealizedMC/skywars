package me.realized.skywars.configuration;

import me.realized.skywars.Core;
import me.realized.skywars.utilities.ItemUtil;
import me.realized.skywars.utilities.MyChest;
import me.realized.skywars.utilities.MyItem;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyConfig {

    private final FileConfiguration config;

    private int duration = 600;
    private List<Integer> resets = Arrays.asList(180, 300);
    private int range = 15;
    private MyChest island;
    private MyChest middle;

    public MyConfig(Core instance) {
        File file = new File(instance.getDataFolder(), "config.yml");

        if (!file.exists()) {
            instance.saveResource("config.yml", true);
        }

        this.config = YamlConfiguration.loadConfiguration(file);
        load();
    }

    private void load() {
        if (config.isInt("game.duration")) {
            duration = config.getInt("game.duration");
        }

        if (config.isList("game.chests.refills")) {
            resets = config.getIntegerList("game.chests.refills");
        }

        if (config.isInt("game.chests.range")) {
            range = config.getInt("game.chests.range");
        }

        List<String> islandChest = new ArrayList<>();

        if (config.isList("game.chests.island-chest.contents")) {
            islandChest = config.getStringList("game.chests.island-chest.contents");
        }

        List<MyItem> islandContents = new ArrayList<>();

        for (String line : islandChest) {
            MyItem item = ItemUtil.toItem(line);

            if (item == null) {
                continue;
            }

            islandContents.add(item);
        }

        island = new MyChest(MyChest.ChestType.ISLAND, islandContents);

        List<String> middleChest = new ArrayList<>();

        if (config.isList("game.chests.middle-chest.contents")) {
            middleChest = config.getStringList("game.chests.middle-chest.contents");
        }

        List<MyItem> middleContents = new ArrayList<>();

        for (String line : middleChest) {
            MyItem item = ItemUtil.toItem(line);

            if (item == null) {
                continue;
            }

            middleContents.add(item);
        }

        middle = new MyChest(MyChest.ChestType.MIDDLE, middleContents);
    }

    public int getGameDuration() {
        return duration;
    }

    public List<Integer> getChestResetTimings() {
        return resets;
    }

    public int getRange() {
        return range;
    }
                                                                                  
    public MyChest getChest(MyChest.ChestType type) {
        switch (type) {
            case ISLAND:
                return island;
            case MIDDLE:
                return middle;
        }

        return null;
    }

    public FileConfiguration getConfig() {
        return config;
    }
}
