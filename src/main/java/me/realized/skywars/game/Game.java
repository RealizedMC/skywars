package me.realized.skywars.game;

import me.realized.skywars.players.GamePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private final GameArena arena;
    private long start;
    private GameState state;
    private List<GamePlayer> players = new ArrayList<>();

    public Game(GameArena arena) {
        this.arena = arena;
        this.start = System.currentTimeMillis();
        this.state = GameState.PREPARING;
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public void addPlayer(GamePlayer player) {
        players.add(player);
    }

    public void removePlayer(GamePlayer player) {
        players.remove(player);
    }

    public List<GamePlayer> getPlayers() {
        return players;
    }

    public List<GamePlayer> getAlivePlayers() {
        List<GamePlayer> alive = new ArrayList<>();

        for (GamePlayer player : players) {
            if (player.isOnline() && !player.isSpectating()) {
                alive.add(player);
            }
        }

        return alive;
    }

    public List<GamePlayer> getSpectators() {
        List<GamePlayer> spectators = new ArrayList<>();

        for (GamePlayer player : players) {
            if (player.isOnline() && player.isSpectating()) {
                spectators.add(player);
            }
        }

        return spectators;
    }

    public GamePlayer getWinner() {
        if (getAlivePlayers().size() == 1) {
            return getAlivePlayers().get(0);
        }

        GamePlayer winner = null;

        for (GamePlayer player : getAlivePlayers()) {
            Player base = player.getBase();

            if (winner == null) {
                winner = player;
            } else {
                if (base.getLocation().distance(arena.getCenter()) < winner.getBase().getLocation().distance(arena.getCenter())) {
                    winner = player;
                }
            }
        }

        return winner;
    }

    public GameArena getArena() {
        return arena;
    }

    public int size() {
        return getAlivePlayers().size();
    }

    public long getStartTimeMillis() {
        return start;
    }

    public void setStartTimeMillis(long value) {
        this.start = value;
    }
}
