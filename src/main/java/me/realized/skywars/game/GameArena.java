package me.realized.skywars.game;

import me.realized.skywars.configuration.MyConfig;
import me.realized.skywars.utilities.LocationUtil;
import me.realized.skywars.utilities.MyBlock;
import me.realized.skywars.utilities.MyChest;
import me.realized.skywars.utilities.MyLocation;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;

import java.util.*;

public class GameArena {

    private final MyConfig config;
    private final Location center;
    private final GameMap map;
    private final World world;

    private Map<Location, Double> chests = new HashMap<>();
    private List<Cage> cages = new ArrayList<>();
    private int size = 0;

    public GameArena(MyConfig config, Location center, GameMap map) {
        this.config = config;
        this.center = center;
        this.world = center.getWorld();
        this.map = map;
    }

    @SuppressWarnings("deprecation")
    public void load() throws IllegalArgumentException {
        if (world == null) {
            throw new IllegalArgumentException("World does not exist.");
        }

        MyLocation min = map.getMinimumPoint();
        MyLocation max = map.getMaximumPoint();

        world.getWorldBorder().setCenter(center);
        world.getWorldBorder().setSize(min.distance(max) * 2);

        for (int x = min.getX(); x <= max.getX(); x++) {
            for (int y = min.getY(); y <= max.getY(); y++) {
                for (int z = min.getZ(); z <= max.getZ(); z++) {
                    Chunk chunk = world.getChunkAt(x, z);

                    if (!chunk.isLoaded()) {
                        chunk.load();
                    }

                    Block block = world.getBlockAt(x, y, z);

                    if (block != null && block.getType() != Material.AIR) {
                        block.setType(Material.AIR);
                    }
                }
            }
        }

        for (MyBlock myBlock : map.getBlocks()) {
            MyLocation myLocation = myBlock.getLocation();
            Block block = world.getBlockAt(myLocation.getX(), myLocation.getY(), myLocation.getZ());
            Location location = block.getLocation().clone();

            block.setType(myBlock.getType());
            block.setData(myBlock.getData());

            switch (block.getType()) {
                case CHEST:
                    double distance = location.distance(center);

                    chests.put(location, distance);
                    refill((Chest) block.getState(), distance);
                    break;
                case BEACON:
                    List<Location> box = LocationUtil.box(location);

                    Cage cage = new Cage(location, box);
                    cages.add(cage);

                    for (Location boxLocation : box) {
                        world.getBlockAt(boxLocation).setType(Material.STAINED_GLASS);
                    }

                    size++;
                    break;
            }
        }

        if (size < 2) {
            throw new IllegalArgumentException("Not enough spawn locations found in the arena! (" + size + ")");
        }
    }

    private void refill(Chest chest, double distance) {
        MyChest.ChestType type;

        if (distance <= config.getRange()) {
            type = MyChest.ChestType.MIDDLE;
        } else {
            type = MyChest.ChestType.ISLAND;
        }

        MyChest myChest = config.getChest(type);
        myChest.refill(chest);
    }

    public void refill() {
        if (!chests.isEmpty()) {
            for (Map.Entry<Location, Double> entry : chests.entrySet()) {
                Block block = world.getBlockAt(entry.getKey());

                if (block != null && block.getType() != Material.AIR && block.getState() instanceof Chest) {
                    refill((Chest) block.getState(), entry.getValue());
                }
            }
        }
    }

    public Location getFirstEmpty(Player player) {
        for (Cage cage : cages) {
            if (cage.getOwner() == null) {
                cage.setOwner(player.getUniqueId());
                return cage.getBlock().clone();
            }
        }

        return null;
    }

    public void setUnused(Player player) {
        for (Cage cage : cages) {
            if (cage.getOwner() != null && cage.getOwner().equals(player.getUniqueId())) {
                cage.setOwner(null);
            }
        }
    }

    public void openCages() {
        for (Cage cage : cages) {
            for (Location blocks : cage.getBox()) {
                blocks.getBlock().setType(Material.AIR);
            }
        }
    }

    public Location getCenter() {
        return center;
    }

    public int size() {
        return size;
    }

    private class Cage {

        private final Location block;
        private final List<Location> box;
        private UUID owner = null;

        public Cage(Location block, List<Location> box) {
            this.block = block;
            this.box = box;
        }

        public Location getBlock() {
            return block;
        }

        public List<Location> getBox() {
            return box;
        }

        public UUID getOwner() {
            return owner;
        }

        public void setOwner(UUID owner) {
            this.owner = owner;
        }
    }
}
