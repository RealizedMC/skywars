package me.realized.skywars.game;

import me.realized.skywars.Core;
import me.realized.skywars.configuration.MyConfig;
import me.realized.skywars.players.DataManager;
import me.realized.skywars.players.GamePlayer;
import me.realized.skywars.utilities.LocationUtil;
import me.realized.skywars.utilities.MyLocation;
import me.realized.skywars.utilities.NMSUtil;
import me.realized.skywars.utilities.StringUtil;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.potion.PotionEffect;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;
import java.util.logging.Level;

public class GameManager {

    private final Core instance;
    private final MyConfig config;
    private final DataManager manager;

    private Game game = null;

    private Map<UUID, UUID> lastDamager = new HashMap<>();
    private Map<UUID, Long> damageTime = new HashMap<>();

    public GameManager(Core instance) {
        this.instance = instance;
        this.config = instance.getConfiguration();
        this.manager = instance.getDataManager();
    }

    public boolean load() {
        World world = loadWorld("skywars");
        setWorldOptions(world);

        File file = new File(instance.getDataFolder(), "arena.dat");

        if (!file.exists()) {
            instance.warn("Failed to find arena file. To create one, use the following command: /skywars setup box");
            return false;
        }

        GameMap map;

        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(file))) {
            map = (GameMap) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            instance.warn("An error occured while trying to load arena file! (" + e.getMessage() + ")");
            return false;
        }

        Location center;

        if (config.getConfig().isString("center")) {
            center = LocationUtil.from(config.getConfig().getString("center"));
        } else {
            MyLocation min = map.getMinimumPoint();
            MyLocation max = map.getMaximumPoint();
            center = new Location(world, (min.getX() + max.getX()) / 2, (min.getY() + max.getY()) / 2, (min.getZ() + max.getZ()) / 2);
        }

        game = new Game(new GameArena(config, center, map));

        try {
            game.getArena().load();
        } catch (IllegalArgumentException e) {
            instance.warn(e.getMessage());
            return false;
        }

        for (Chunk chunk : world.getLoadedChunks()) {
            for (Entity entity : chunk.getEntities()) {
                if (!(entity instanceof LivingEntity)) {
                    entity.remove();
                }
            }
        }

        Bukkit.getScheduler().runTaskTimer(instance, new GameUpdater(game, instance), 0L, 20L);
        game.setState(GameState.PREGAME);
        return true;
    }

    public Game getCurrentGame() {
        return game;
    }

    public void handleLogin(AsyncPlayerPreLoginEvent event) {
        if (game.size() >= game.getArena().size() || game.getState() != GameState.PREGAME) {
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, StringUtil.color("&cThis game has already started!"));
        }
    }

    public void handleJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);

        Player player = event.getPlayer();

        if (player.isDead()) {
            player.spigot().respawn();
        }

        player.setHealth(20.0D);
        player.setFoodLevel(20);

        player.setGameMode(GameMode.SURVIVAL);
        player.getInventory().clear();
        player.getInventory().setArmorContents(new ItemStack[4]);

        player.setExp(0F);
        player.setLevel(0);

        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }

        GamePlayer gamePlayer = manager.getPlayer(player.getUniqueId());

        game.addPlayer(gamePlayer);

        instance.getBoardManager().createScoreboard(player);
        instance.getBoardManager().refreshAll();

        Location box = game.getArena().getFirstEmpty(player).add(0, 1, 0);

        LocationUtil.center(box);
        box.setDirection(LocationUtil.point(box, game.getArena().getCenter()));
        player.teleport(box);
        player.playSound(player.getLocation(), Sound.NOTE_PIANO, 1.0F, 1.4F);

        String msg = StringUtil.format("&f%player% &7has joined. (&f%size%&7/&f%max%&7)", "%player%", player.getName(), "%size%", game.size(), "%max%", game.getArena().size());
        Bukkit.broadcastMessage(StringUtil.color(msg));
    }

    public void handleQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);

        Player player = event.getPlayer();
        GamePlayer gamePlayer = manager.getPlayer(player.getUniqueId());

        if (gamePlayer == null) {
            return;
        }

        game.removePlayer(gamePlayer);

        instance.getBoardManager().refreshAll();
        instance.getBoardManager().removeScoreboard(player);

        switch (game.getState()) {
            case PREGAME:
                game.getArena().setUnused(player);
                String msg = StringUtil.format("&f%player% &7has left. (&f%size%&7/&f%max%&7)", "%player%", player.getName(), "%size%", game.size(), "%max%", game.getArena().size());
                Bukkit.broadcastMessage(StringUtil.color(msg));
                break;
            case INGAME:
                gamePlayer.setCause(GamePlayer.DeathCause.LOGOUT);
                player.setHealth(0.0D);
                break;
        }
    }

    public void handleDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getEntity();
        GamePlayer gPlayer = manager.getPlayer(player.getUniqueId());

        if (gPlayer == null) {
            return;
        }

        if (game.getState() != GameState.INGAME || gPlayer.isSpectating()) {
            event.setCancelled(true);
            return;
        }

        Player damager = null;
        GamePlayer gDamager;
        EntityDamageByEntityEvent damageEvent;

        switch (event.getCause()) {
            case FALL:
                if (game.getState() == GameState.INGAME && game.getStartTimeMillis() + 3000 - System.currentTimeMillis() > 0) {
                    event.setCancelled(true);
                    return;
                }

                break;
            case VOID:
                event.setDamage(1000);
                break;
            case ENTITY_ATTACK:
                damageEvent = (EntityDamageByEntityEvent) event;

                if (damageEvent.getDamager() instanceof Player) {
                    damager = (Player) damageEvent.getDamager();
                    gDamager = manager.getPlayer(damager.getUniqueId());

                    if (gDamager == null) {
                        return;
                    }

                    if (gDamager.isSpectating()) {
                        event.setCancelled(true);
                        return;
                    }
                }

                break;
            case PROJECTILE:
                damageEvent = (EntityDamageByEntityEvent) event;

                if (damageEvent.getDamager() instanceof Projectile && ((Projectile) damageEvent.getDamager()).getShooter() instanceof Player) {
                    damager = (Player) ((Projectile) damageEvent.getDamager()).getShooter();
                }

                break;
            case MAGIC:
                damageEvent = (EntityDamageByEntityEvent) event;

                if (damageEvent.getDamager() instanceof Projectile && ((Projectile) damageEvent.getDamager()).getShooter() instanceof Player) {
                    damager = (Player) ((Projectile) damageEvent.getDamager()).getShooter();
                }

                break;
        }

        if (damager != null && !damager.equals(player)) {
            lastDamager.put(player.getUniqueId(), damager.getUniqueId());
            damageTime.put(player.getUniqueId(), System.currentTimeMillis());
        }
    }

    public void handleDeath(PlayerDeathEvent event) {
        event.setDroppedExp(0);
        event.getDrops().clear();

        Player player = event.getEntity();
        UUID uuid = player.getUniqueId();
        GamePlayer gPlayer = manager.getPlayer(player.getUniqueId());

        if (gPlayer != null) {
            gPlayer.edit(GamePlayer.StatsType.DEATHS, GamePlayer.EditType.ADD, 1);
            gPlayer.setSpectating(true);
            player.setGameMode(GameMode.SPECTATOR);
            player.setHealth(player.getMaxHealth());
            player.setFoodLevel(20);
            player.teleport(game.getArena().getCenter().clone().add(0, 10, 0));
            NMSUtil.title(player, StringUtil.color("&c&lYOU DIED!"), StringUtil.color("&7You are now a spectator."), 10, 30, 10, NMSUtil.TitleAction.DISPLAY);
            instance.getBoardManager().refreshAll();

            UUID damager = lastDamager.get(player.getUniqueId());

            if (damager == null || Bukkit.getPlayer(damager) == null || (damageTime.get(uuid) + 7500 - System.currentTimeMillis() <= 0)) {
                damager = null;
                lastDamager.remove(uuid);
                damageTime.remove(uuid);
            }

            Player killer = null;
            EntityDamageEvent lastDamageCause = player.getLastDamageCause();

            if (lastDamageCause != null && gPlayer.getCause() != GamePlayer.DeathCause.LOGOUT) {
                switch (lastDamageCause.getCause()) {
                    case ENTITY_ATTACK:
                        if (damager == null) {
                            gPlayer.setCause(GamePlayer.DeathCause.OTHER);
                            break;
                        }

                        killer = Bukkit.getPlayer(damager);
                        gPlayer.setCause(GamePlayer.DeathCause.SLAIN);
                        break;
                    case VOID:
                        if (damager == null) {
                            gPlayer.setCause(GamePlayer.DeathCause.VOID);
                            break;
                        }

                        killer = Bukkit.getPlayer(damager);
                        gPlayer.setCause(GamePlayer.DeathCause.VOID_OTHER);
                        break;
                    case FALL:
                        if (damager == null) {
                            gPlayer.setCause(GamePlayer.DeathCause.FALL);
                            break;
                        }

                        killer = Bukkit.getPlayer(damager);
                        gPlayer.setCause(GamePlayer.DeathCause.FALL_OTHER);
                        break;
                    case PROJECTILE:
                        if (damager == null) {
                            gPlayer.setCause(GamePlayer.DeathCause.OTHER);
                            break;
                        }

                        killer = Bukkit.getPlayer(damager);
                        gPlayer.setCause(GamePlayer.DeathCause.SHOT);
                        break;

                    case MAGIC:
                        if (damager == null) {
                            gPlayer.setCause(GamePlayer.DeathCause.OTHER);
                            break;
                        }

                        killer = Bukkit.getPlayer(damager);
                        gPlayer.setCause(GamePlayer.DeathCause.SPLASH);
                        break;
                    case FIRE_TICK:
                        if (damager == null) {
                            gPlayer.setCause(GamePlayer.DeathCause.BURNT);
                            break;
                        }

                        killer = Bukkit.getPlayer(damager);
                        gPlayer.setCause(GamePlayer.DeathCause.BURNT_OTHER);
                        break;
                    case FIRE:
                        if (damager == null) {
                            gPlayer.setCause(GamePlayer.DeathCause.BURNT);
                            break;
                        }

                        killer = Bukkit.getPlayer(damager);
                        gPlayer.setCause(GamePlayer.DeathCause.BURNT_OTHER);
                        break;
                    case LAVA:
                        if (damager == null) {
                            gPlayer.setCause(GamePlayer.DeathCause.LAVA);
                            break;
                        }

                        killer = Bukkit.getPlayer(damager);
                        gPlayer.setCause(GamePlayer.DeathCause.LAVA_OTHER);
                        break;
                    case BLOCK_EXPLOSION:
                        if (damager == null) {
                            gPlayer.setCause(GamePlayer.DeathCause.BLEW_UP);
                            break;
                        }

                        killer = Bukkit.getPlayer(damager);
                        gPlayer.setCause(GamePlayer.DeathCause.BLEW_UP_OTHER);
                        break;
                    case ENTITY_EXPLOSION:
                        if (damager == null) {
                            gPlayer.setCause(GamePlayer.DeathCause.BLEW_UP);
                            break;
                        }

                        killer = Bukkit.getPlayer(damager);
                        gPlayer.setCause(GamePlayer.DeathCause.BLEW_UP_OTHER);
                        break;
                }
            }

            if (killer != null) {
                Bukkit.broadcastMessage(StringUtil.color(StringUtil.format(gPlayer.getCause().getDeathMessage(), "%killed%", player.getName(), "%killer%", killer.getName())));
                killer.playSound(killer.getLocation(), Sound.ORB_PICKUP, 1.0F, 0.1F);

                GamePlayer gpKiller = manager.getPlayer(killer.getUniqueId());

                if (gpKiller != null) {
                    gpKiller.edit(GamePlayer.StatsType.KILLS, GamePlayer.EditType.ADD, 1);
                }
            } else {
                Bukkit.broadcastMessage(StringUtil.color(StringUtil.format(gPlayer.getCause().getDeathMessage(), "%killed%", player.getName())));
            }
        }

        if (game.size() <= 1) {
            game.setState(GameState.ENDED);
            GamePlayer winner = game.getAlivePlayers().get(0);
            Player base = winner.getBase();

            if (base != null) {
                Firework firework = (Firework) base.getWorld().spawnEntity(base.getEyeLocation(), EntityType.FIREWORK);
                FireworkMeta meta = firework.getFireworkMeta();
                meta.setPower(5);
                meta.addEffect(FireworkEffect.builder().withColor(Color.WHITE).with(FireworkEffect.Type.BALL_LARGE).build());
                firework.setFireworkMeta(meta);
                NMSUtil.title(base, StringUtil.color("&6&lVICTORY!"), StringUtil.color("&7You are the last survivor!"), 10, 30, 10, NMSUtil.TitleAction.DISPLAY);
            }
        }
    }

    public void handleBreak(BlockBreakEvent event) {
        if (game.getState() != GameState.PREGAME) {
            return;
        }

        event.setCancelled(true);
        event.getPlayer().sendMessage(StringUtil.color("&cYou may not destroy blocks before the game start."));
    }

    private World loadWorld(String name) {
        WorldCreator worldCreator = new WorldCreator(name);

        worldCreator.environment(World.Environment.NORMAL);
        worldCreator.generateStructures(false);
        worldCreator.generator(new ChunkGenerator() {
            public List<BlockPopulator> getDefaultPopulators(World world) {
                return Arrays.asList(new BlockPopulator[0]);
            }

            public boolean canSpawn(World world, int x, int z) {
                return true;
            }

            @SuppressWarnings("deprecation")
            public byte[] generate(World world, Random rand, int x, int z) {
                return new byte[32768];
            }

            public Location getFixedSpawnLocation(World world, Random random) {
                return new Location(world, 0.0D, 128.0D, 0.0D);
            }
        });

        return worldCreator.createWorld();
    }

    private void setWorldOptions(World world) {
        world.setDifficulty(Difficulty.NORMAL);
        world.setSpawnFlags(true, true);
        world.setPVP(true);
        world.setStorm(false);
        world.setThundering(false);
        world.setKeepSpawnInMemory(true);
        world.setTicksPerAnimalSpawns(0);
        world.setTicksPerMonsterSpawns(0);

        world.setGameRuleValue("doMobSpawning", "false");
        world.setGameRuleValue("mobGriefing", "false");
        world.setGameRuleValue("doFireTick", "false");
        world.setGameRuleValue("showDeathMessages", "false");
    }
}