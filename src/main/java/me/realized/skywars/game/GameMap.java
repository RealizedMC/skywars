package me.realized.skywars.game;

import me.realized.skywars.utilities.MyBlock;
import me.realized.skywars.utilities.MyLocation;

import java.io.Serializable;
import java.util.List;

public class GameMap implements Serializable {

    private static final long serialVersionUID = 5725002325234457417L;

    private final MyLocation minimum;
    private final MyLocation maximum;
    private final List<MyBlock> blocks;

    public GameMap(List<MyBlock> blocks, MyLocation minimum, MyLocation maximum) {
        this.blocks = blocks;
        this.minimum = minimum;
        this.maximum = maximum;
    }

    public MyLocation getMinimumPoint() {
        return minimum;
    }

    public MyLocation getMaximumPoint() {
        return maximum;
    }

    public List<MyBlock> getBlocks() {
        return blocks;
    }
}
