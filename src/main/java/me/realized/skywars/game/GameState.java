package me.realized.skywars.game;

public enum GameState {

    PREPARING, PREGAME, INGAME, ENDED
}
