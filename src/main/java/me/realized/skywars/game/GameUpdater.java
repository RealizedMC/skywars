package me.realized.skywars.game;

import me.realized.skywars.Core;
import me.realized.skywars.configuration.MyConfig;
import me.realized.skywars.players.GamePlayer;
import me.realized.skywars.scoreboard.BoardManager;
import me.realized.skywars.utilities.DateUtil;
import me.realized.skywars.utilities.NMSUtil;
import me.realized.skywars.utilities.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

import java.util.ArrayList;
import java.util.List;

class GameUpdater implements Runnable {

    private final Game game;
    private final Core instance;
    private final MyConfig config;

    private int timer = 5;
    private boolean maxed = false;
    private boolean displayed = false;

    public GameUpdater(Game game, Core instance) {
        this.game = game;
        this.instance = instance;
        this.config = instance.getConfiguration();
    }

    @Override
    public void run() {
        BoardManager manager;

        switch (game.getState()) {
            case PREGAME:
                int online = game.size();

                if (game.getArena().size() == online && !maxed) {
                    maxed = true;
                }

                if (maxed) {
                    String title;
                    String subTitle;
                    String message;

                    if (timer <= 0) {
                        timer = 5;
                        int minSize = game.getArena().size() / 2 + (game.getArena().size() % 2 > 0 ? 1 : 0);

                        if (game.size() < (minSize != 1 ? minSize : 2)) {
                            Bukkit.broadcastMessage(StringUtil.color("&7Not enough players! Resetting the timer."));
                            maxed = false;
                            return;
                        }

                        game.setState(GameState.INGAME);
                        game.setStartTimeMillis(System.currentTimeMillis());
                        game.getArena().openCages();
                        title = "";
                        subTitle = StringUtil.color("&7Cages opened, &cFIGHT&7!");
                        message = StringUtil.color("&7Cages opened, &cFIGHT&7!");
                    } else {
                        title = StringUtil.color("&l" + timer);
                        subTitle = StringUtil.color("&7&lGET READY!");
                        message = StringUtil.color("&7Game starting in &f" + timer + "&7 second" + (timer != 1 ? "s" : "") + "!");
                        timer--;
                    }

                    Bukkit.broadcastMessage(message);

                    for (GamePlayer player : game.getPlayers()) {
                        if (player.getBase() != null) {
                            if (!title.equals("")) {
                                player.getBase().playSound(player.getBase().getLocation(), Sound.FIRE_IGNITE, 1.0F, 0.1F);
                                NMSUtil.title(player.getBase(), title, subTitle, 0, 30, 0, NMSUtil.TitleAction.DISPLAY);
                            } else {
                                player.getBase().playSound(player.getBase().getLocation(), Sound.NOTE_PIANO, 1.0F, 0.7F);
                                NMSUtil.title(player.getBase(), null, null, -1, -1, -1, NMSUtil.TitleAction.CLEAR);
                                NMSUtil.title(player.getBase(), title, subTitle, 10, 40, 10, NMSUtil.TitleAction.DISPLAY);
                            }
                        }
                    }
                }

                break;
            case INGAME:
                long ms = game.getStartTimeMillis() + (config.getGameDuration() * 1000) - System.currentTimeMillis();
                manager = instance.getBoardManager();

                if (manager != null) {
                    manager.updateTitles("&b&lSKYWARS &7(" + DateUtil.format(ms) + ")");
                }

                if (!config.getChestResetTimings().isEmpty()) {
                    for (int time : config.getChestResetTimings()) {
                        if ((ms / 1000) == time) {
                            game.getArena().refill();

                            for (GamePlayer player : game.getPlayers()) {
                                if (player.getBase() != null) {
                                    player.getBase().playSound(player.getBase().getLocation(), Sound.CHEST_OPEN, 1.0F, 1.0F);
                                    NMSUtil.title(player.getBase(), "",StringUtil.color("&6&lCHESTS &7HAVE BEEN RESET!"), 10, 30, 10, NMSUtil.TitleAction.DISPLAY);
                                }
                            }
                        }
                    }
                }

                if (ms <= 0) {
                    game.setState(GameState.ENDED);
                }

                break;
            case ENDED:
                if (!displayed) {
                    displayed = true;
                    GamePlayer winner = game.getWinner();
                    manager = instance.getBoardManager();

                    if (manager != null) {
                        manager.updateTitles("&b&lSKYWARS");
                    }

                    if (winner != null) {
                        Firework firework = (Firework) winner.getBase().getWorld().spawnEntity(winner.getBase().getEyeLocation(), EntityType.FIREWORK);
                        FireworkMeta meta = firework.getFireworkMeta();
                        meta.setPower(5);
                        meta.addEffect(FireworkEffect.builder().withColor(Color.WHITE).with(FireworkEffect.Type.BALL_LARGE).build());
                        firework.setFireworkMeta(meta);
                        NMSUtil.title(winner.getBase(), StringUtil.color("&6&lVICTORY!"), StringUtil.color("&7You were the one closest to the center!"), 10, 30, 10, NMSUtil.TitleAction.DISPLAY);
                    }

                    List<String> result = new ArrayList<>();
                    result.add("&f*l∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎");
                    result.add("");
                    result.add("       &7Winner - &f" + (winner != null ? winner.getName() : "none"));
                    result.add("");
                    result.add("&f&l∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎");

                    for (String s : result) {
                        Bukkit.broadcastMessage(StringUtil.color(s));
                    }

                    Bukkit.getScheduler().runTaskLater(instance, new Runnable() {
                        @Override
                        public void run() {
                            Bukkit.shutdown();
                        }
                    }, 20L * 8);
                }

                break;
        }
    }
}