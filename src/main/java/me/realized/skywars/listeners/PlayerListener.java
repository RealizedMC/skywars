package me.realized.skywars.listeners;

import me.realized.skywars.Core;
import me.realized.skywars.game.GameManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    private final GameManager manager;

    public PlayerListener(Core instance) {
        this.manager = instance.getGameManager();
    }

    @EventHandler
    public void onLogin(AsyncPlayerPreLoginEvent event) {
        manager.handleLogin(event);
    }

    @EventHandler (priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent event) {
        manager.handleJoin(event);
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onQuit(PlayerQuitEvent event) {
        manager.handleQuit(event);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        manager.handleBreak(event);
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        manager.handleDamage(event);
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onDeath(PlayerDeathEvent event) {
        manager.handleDeath(event);
    }
}
