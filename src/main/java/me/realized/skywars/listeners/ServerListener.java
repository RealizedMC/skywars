package me.realized.skywars.listeners;

import me.realized.skywars.Core;
import me.realized.skywars.game.GameManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerListener implements Listener {

    private final GameManager manager;

    public ServerListener(Core instance) {
        this.manager = instance.getGameManager();
    }

    @EventHandler
    public void onPing(ServerListPingEvent event) {
        if (manager.getCurrentGame() != null) {
            event.setMotd("GameState: " + manager.getCurrentGame().getState());
        }
    }
}
