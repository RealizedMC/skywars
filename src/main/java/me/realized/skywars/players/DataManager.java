package me.realized.skywars.players;

import me.realized.skywars.Core;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class DataManager implements Listener {

    private Map<UUID, GamePlayer> players = new ConcurrentHashMap<>();

    public DataManager(Core instance) {
        Bukkit.getPluginManager().registerEvents(this, instance);
    }

    public GamePlayer getPlayer(UUID uuid) {
        return players.get(uuid);
    }

    /* For later uses
    public List<GamePlayer> getTopKills() {
        List<GamePlayer> topPlayers = new ArrayList<>(players.values());

        Collections.sort(topPlayers, new Comparator<GamePlayer>() {
            @Override
            public int compare(GamePlayer p1, GamePlayer p2) {
                return Integer.valueOf(p2.get(GamePlayer.StatsType.KILLS)).compareTo(p1.get(GamePlayer.StatsType.KILLS));
            }
        });

        if (topPlayers.size() > 3) {
            Iterator<GamePlayer> iterator = topPlayers.iterator();

            while (iterator.hasNext()) {
                int index = topPlayers.indexOf(iterator.next());

                if (index > 2) {
                    iterator.remove();
                }
            }
        }

        return topPlayers;
    }
    */

    @EventHandler (priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);

        UUID uuid = event.getPlayer().getUniqueId();
        players.put(uuid, new GamePlayer(event.getPlayer()));
    }
}
