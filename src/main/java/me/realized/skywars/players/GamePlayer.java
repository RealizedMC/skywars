package me.realized.skywars.players;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class GamePlayer {

    private final UUID uuid;
    private final String name;
    private final Player base;
    private boolean spectator = false;
    private int wins = 0;
    private int losses = 0;
    private int kills = 0;
    private int deaths = 0;
    private int score = 0;
    private DeathCause cause = DeathCause.OTHER;

    public GamePlayer(Player base) {
        this.uuid = base.getUniqueId();
        this.name = base.getName();
        this.base = base;
    }

    public UUID getUUID() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public Player getBase() {
        return base;
    }

    public boolean isOnline() {
        return Bukkit.getPlayer(uuid) != null;
    }

    public boolean isSpectating() {
        return spectator;
    }

    public void setSpectating(boolean spectator) {
        this.spectator = spectator;
    }

    public int get(StatsType type) {
        switch (type) {
            case WINS:
                return wins;
            case LOSSES:
                return losses;
            case KILLS:
                return kills;
            case DEATHS:
                return deaths;
            case SCORE:
                return score;
            default:
                return 0;
        }
    }

    public void edit(StatsType statsType, EditType editType, int amount) {
        switch (statsType) {
            case WINS:
                switch (editType) {
                    case ADD:
                        wins += amount;
                        break;
                    case REMOVE:
                        wins -= amount;
                        break;
                    case SET:
                        wins = amount;
                        break;
                }
                break;
            case LOSSES:
                switch (editType) {
                    case ADD:
                        losses += amount;
                        break;
                    case REMOVE:
                        losses -= amount;
                        break;
                    case SET:
                        losses = amount;
                        break;
                }
                break;
            case KILLS:
                switch (editType) {
                    case ADD:
                        kills += amount;
                        break;
                    case REMOVE:
                        kills -= amount;
                        break;
                    case SET:
                        kills = amount;
                        break;
                }
                break;
            case DEATHS:
                switch (editType) {
                    case ADD:
                        deaths += amount;
                        break;
                    case REMOVE:
                        deaths -= amount;
                        break;
                    case SET:
                        deaths = amount;
                        break;
                }
                break;
            case SCORE:
                switch (editType) {
                    case ADD:
                        score += amount;
                        break;
                    case REMOVE:
                        score -= amount;
                        break;
                    case SET:
                        score = amount;
                        break;
                }

                break;
        }
    }

    public DeathCause getCause() {
        return cause;
    }

    public void setCause(DeathCause cause) {
        this.cause = cause;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GamePlayer that = (GamePlayer) o;

        return !(uuid != null ? !uuid.equals(that.uuid) : that.uuid != null) && !(name != null ? !name.equals(that.name) : that.name != null) && !(base != null ? !base.equals(that.base) : that.base != null);

    }

    @Override
    public int hashCode() {
        int result = uuid != null ? uuid.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (base != null ? base.hashCode() : 0);
        return result;
    }

    public enum EditType {
        ADD, REMOVE, SET;

        public static boolean isValue(String input) {
            for (EditType type : values()) {
                if (type.name().equals(input)) {
                    return true;
                }
            }

            return false;
        }
    }

    public enum StatsType {
        WINS, LOSSES, KILLS, DEATHS, SCORE;

        public static boolean isValue(String input) {
            for (StatsType type : values()) {
                if (type.name().equals(input)) {
                    return true;
                }
            }

            return false;
        }
    }

    public enum DeathCause {

        SLAIN("&f%killed% &7was slain by &f%killer%&7."),
        BLEW_UP("%7f%killed% &7blew up."),
        BLEW_UP_OTHER("%&f%killed% &7blew up whilst fighting &f%killer%&7."),
        BURNT("&f%killed% &7burned to death."),
        BURNT_OTHER("&f%killed% &7was burnt to a crisp whilst fighting &f%killer%&7."),
        LAVA("&f%killed% &7tried to swim in lava."),
        LAVA_OTHER("&f%killed% &7tried to swim in lava while trying to escape &f%killer%&7."),
        SPLASH("&f%killed% &7was killed by &f%killer% &7using harming potions&7."),
        SHOT("&f%killed% &7was shot by &f%killer%&7."),
        VOID("&f%killed% &7fell into the void."),
        VOID_OTHER("&f%killed% &7was thorn into the void by &f%killer%&7."),
        FALL("&f%killed% &7hit the ground too hard."),
        FALL_OTHER("&f%killed% &7was smashed on the ground by &f%killer%&7."),
        LOGOUT("&f%killed% &7was killed for logging out."),
        OTHER("&f%killed% &7died.");

        private final String message;

        DeathCause(String message) {
            this.message = message;
        }

        public String getDeathMessage() {
            return message;
        }
    }
}
