package me.realized.skywars.scoreboard;

import me.realized.skywars.Core;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.ScoreboardManager;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BoardManager {

    private final Core instance;
    private final ScoreboardManager manager = Bukkit.getScoreboardManager();

    private Map<UUID, Integer> kills = new HashMap<>();
    private Map<UUID, MyScoreboard> boards = new HashMap<>();

    public BoardManager(Core instance) {
        this.instance = instance;
    }

    public void createScoreboard(Player player) {
        UUID uuid = player.getUniqueId();
        MyScoreboard board = new MyScoreboard(instance, player).refresh();
        boards.put(uuid, board);
    }

    public void removeScoreboard(Player player) {
        player.setScoreboard(manager.getNewScoreboard());
        boards.remove(player.getUniqueId());
        kills.remove(player.getUniqueId());
    }

    public void refreshAll() {
        for (UUID uuid : boards.keySet()) {
            if (Bukkit.getPlayer(uuid) == null) {
                continue;
            }

            boards.get(uuid).refresh();
        }
    }

    public void updateTitles(String title) {
        for (UUID uuid : boards.keySet()) {
            if (Bukkit.getPlayer(uuid) == null) {
                continue;
            }

            boards.get(uuid).setTitle(title);
        }
    }

    public void increaseKills(Player player) {
        UUID uuid = player.getUniqueId();
        int kills = this.kills.get(uuid) != null ? this.kills.get(uuid) + 1 : 1;
        this.kills.put(uuid, kills);
    }

    public int getKills(Player player) {
        return kills.get(player.getUniqueId()) != null ? kills.get(player.getUniqueId()) : 0;
    }
}
