package me.realized.skywars.scoreboard;

import me.realized.skywars.Core;
import me.realized.skywars.utilities.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.ArrayList;
import java.util.List;

public class MyScoreboard {

    private final Core instance;
    private final Player base;
    private final Scoreboard board;

    private int count;
    private Objective objective;

    public MyScoreboard(Core instance, Player base) {
        this.instance = instance;
        this.base = base;
        this.board = Bukkit.getScoreboardManager().getNewScoreboard();
        this.objective = board.registerNewObjective("sidebar", "dummy");

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(StringUtil.color("&b&lSKYWARS"));
        base.setScoreboard(board);
    }

    public MyScoreboard refresh() {
        List<String> lines = new ArrayList<>();

        add(lines, " ");
        add(lines, "&7by Realized :)");
        add(lines, " ");
        add(lines, "Players: &b" + instance.getGameManager().getCurrentGame().size());
        add(lines, " ");
        add(lines, "Spectators: &b" + instance.getGameManager().getCurrentGame().getSpectators().size());
        add(lines, " ");
        add(lines, "Kills: &b" + instance.getBoardManager().getKills(base));
        add(lines, " ");
        add(lines, "&6http://play.skywars.here");

        Objective buffer = board.registerNewObjective(String.valueOf(count++), "dummy");

        buffer.setDisplayName(objective.getDisplayName());

        int index = lines.size();

        for (String line : lines) {
            line = StringUtil.color(line);
            buffer.getScore(line).setScore(index);
            index--;
        }

        buffer.setDisplaySlot(objective.getDisplaySlot());

        objective.unregister();
        objective = buffer;
        return this;
    }

    public void setTitle(String title) {
        objective.setDisplayName(StringUtil.color(title));
    }

    private void add(List<String> lines, String line) {
        if (lines.contains(line) && line.contains(" ")) {
            int size = 0;

            for (String s : lines) {
                if (s.contains(line)) {
                    size++;
                }
            }

            for (int i = 0; i < size; i++) {
                line += "&r";
            }
        }

        if (line.length() > 48) {
            return;
        }

        lines.add(line);
    }
}
