package me.realized.skywars.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    private static final SimpleDateFormat format = new SimpleDateFormat("mm:ss");

    public static String format(long ms) {
        if (ms <= 0) {
            return "00:00";
        }

        return format.format(new Date(ms));
    }
}
