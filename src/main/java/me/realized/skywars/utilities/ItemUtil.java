package me.realized.skywars.utilities;

import me.realized.skywars.Core;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

public class ItemUtil {

    private static List<String> enchantments = new ArrayList<>();
    private static List<String> effects = new ArrayList<>();

    static {
        for (Enchantment enchantment : Enchantment.values()) {
            if (enchantment != null) {
                enchantments.add(StringUtil.reverseTranslateEnchantment(enchantment.getName()));
            }
        }

        for (PotionEffectType type : PotionEffectType.values()) {
            if (type != null) {
                effects.add(StringUtil.reverseTranslatePotionEffect(type.getName()));
            }
        }
    }

    @SuppressWarnings("deprecation")
    public static MyItem toItem(String data) {
        try {
            ItemStack item;
            String[] args = data.split(" +");

            if (args.length < 3) {
                return null;
            }

            double weight = Double.parseDouble(args[0]);

            if (weight < 0 || weight > 2) {
                return null;
            }

            String[] type = args[1].split(":");

            if (type.length < 1) {
                return null;
            }

            Material material = Material.getMaterial(Integer.parseInt(type[0]));
            short durability = 0;

            if (type.length > 1) {
                durability = Short.parseShort(type[1]);
            }

            int quantity = Integer.parseInt(args[2]);
            item = new ItemStack(material, quantity, durability);

            if (args.length > 2) {
                for (int i = 2; i < args.length; i++) {
                    addMeta(item, args[i]);
                }
            }

            return new MyItem(weight, item);
        } catch (Exception e) {
            Core.getInstance().warn("An error occurred while trying to parse '" + data + "' to item: " + e.getMessage());
            return null;
        }
    }

    private static void addMeta(ItemStack item, String meta) {
        try {
            String[] args = meta.split(":", 2);
            ItemMeta itemMeta = item.getItemMeta();

            if (args.length < 1) {
                return;
            }

            if (args[0].equalsIgnoreCase("name") || args[0].equalsIgnoreCase("displayname")) {
                itemMeta.setDisplayName(StringUtil.color(args[1].replace("_", " ")));
                item.setItemMeta(itemMeta);
                return;
            }

            if (args[0].equalsIgnoreCase("lore")) {
                List<String> lore = new ArrayList<>();

                for (String st : args[1].split("\\|")) {
                    lore.add(StringUtil.color(st.replace("_", " ")));
                }

                itemMeta.setLore(lore);
                item.setItemMeta(itemMeta);
                return;
            }

            if (enchantments.contains(args[0].toLowerCase())) {
                if (item.getType() == Material.ENCHANTED_BOOK) {
                    EnchantmentStorageMeta enchantmentMeta = (EnchantmentStorageMeta) item.getItemMeta();
                    enchantmentMeta.addStoredEnchant(Enchantment.getByName(StringUtil.translateEnchantment(args[0])), Integer.parseInt(args[1]), true);
                    item.setItemMeta(enchantmentMeta);
                    return;
                }

                item.addUnsafeEnchantment(Enchantment.getByName(StringUtil.translateEnchantment(args[0])), Integer.parseInt(args[1]));
                return;
            }

            if (effects.contains(args[0].toLowerCase()) && item.getType() == Material.POTION) {
                PotionMeta potionMeta = (PotionMeta) itemMeta;
                PotionEffectType type = PotionEffectType.getByName(StringUtil.translatePotionEffect(args[0]));
                int power = Integer.parseInt(args[1].split(":")[0]);
                int duration = Integer.parseInt(args[1].split(":")[1]);
                potionMeta.addCustomEffect(new PotionEffect(type, duration, power), true);
                item.setItemMeta(potionMeta);
                return;
            }

            if ((args[0].equalsIgnoreCase("player") || args[0].equalsIgnoreCase("owner")) && item.getType() == Material.SKULL_ITEM && item.getDurability() == 3) {
                SkullMeta skullMeta = (SkullMeta) itemMeta;
                skullMeta.setOwner(args[1]);
                item.setItemMeta(skullMeta);
            }
        } catch (Exception e) {
            Core.getInstance().warn("An error occurred while trying to apply meta '" + meta + "' to item " + item.getType() + ": " + e.getMessage());
        }
    }
}
