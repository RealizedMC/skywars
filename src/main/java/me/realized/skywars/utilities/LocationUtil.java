package me.realized.skywars.utilities;

import me.realized.skywars.Core;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class LocationUtil {

    public static List<Location> box(Location location) {
        List<Location> result = new ArrayList<>();

        result.add(location);

        result.add(location.clone().add(1, 0, 0));
        result.add(location.clone().add(0, 0, 1));
        result.add(location.clone().add(-1, 0, 0));
        result.add(location.clone().add(0, 0, -1));

        result.add(location.clone().add(1, 1, 0));
        result.add(location.clone().add(0, 1, 1));
        result.add(location.clone().add(-1, 1, 0));
        result.add(location.clone().add(0, 1, -1));

        result.add(location.clone().add(1, 2, 0));
        result.add(location.clone().add(0, 2, 1));
        result.add(location.clone().add(-1, 2, 0));
        result.add(location.clone().add(0, 2, -1));

        result.add(location.clone().add(1, 3, 0));
        result.add(location.clone().add(0, 3, 1));
        result.add(location.clone().add(-1, 3, 0));
        result.add(location.clone().add(0, 3, -1));

        result.add(location.clone().add(0, 4, 0));

        result.add(location.clone().add(1, 4, 0));
        result.add(location.clone().add(0, 4, 1));
        result.add(location.clone().add(-1, 4, 0));
        result.add(location.clone().add(0, 4, -1));
        return result;
    }

    public static Location center(Location location) {
        location.setX(location.getBlockX() + .5);
        location.setZ(location.getBlockZ() + .5);
        return location;
    }

    public static Vector point(Location start, Location end) {
        return end.toVector().subtract(start.toVector()).normalize();
    }

    public static Location from(String data) {
        String[] args = data.split(";");

        if (args.length < 4) {
            return null;
        }

        World world = Bukkit.getWorld(args[0]);

        if (world == null) {
            return null;
        }

        double x;
        double y;
        double z;

        try {
            x = Double.parseDouble(args[1]);
            y = Double.parseDouble(args[2]);
            z = Double.parseDouble(args[3]);
        } catch (NumberFormatException e) {
            Core.getInstance().warn("Failed to load location from '" + data + ": " + e.getMessage());
            return null;
        }

        return new Location(world, x, y, z);
    }

    public static String to(Location location) {
        return location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getZ();
    }

    public static MyLocation toSimpleLocation(Location location) {
        return new MyLocation(location.getBlockX(), location.getBlockY(), location.getBlockZ());
    }
}
