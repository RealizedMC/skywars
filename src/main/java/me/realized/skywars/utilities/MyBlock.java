package me.realized.skywars.utilities;

import org.bukkit.Material;
import org.bukkit.block.Block;

import java.io.Serializable;

public class MyBlock implements Serializable {

    private static final long serialVersionUID = 4484463307149740031L;

    private final Material type;
    private final byte data;
    private final MyLocation simpleLocation;

    @SuppressWarnings("deprecation")
    public MyBlock(Block base) {
        this.type = base.getType();
        this.data = base.getData();
        this.simpleLocation = new MyLocation(base.getLocation());
    }

    public Material getType() {
        return type;
    }

    public byte getData() {
        return data;
    }

    public MyLocation getLocation() {
        return simpleLocation;
    }
}
