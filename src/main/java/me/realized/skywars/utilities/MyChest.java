package me.realized.skywars.utilities;

import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;

import java.util.*;

public class MyChest {

    private final ChestType type;
    private final MyCollection<MyItem> items = new MyCollection<>();

    private static final Random random = new Random();

    public MyChest(ChestType type, List<MyItem> items) {
        this.type = type;

        for (MyItem item : items) {
            this.items.add(item.getWeight(), item);
        }
    }

    public ChestType getType() {
        return type;
    }

    public void refill(Chest chest) {
        Inventory inventory = chest.getBlockInventory();
        inventory.clear();

        Collection<MyItem> contents = new ArrayList<>();
        int chestSize = 3 + random.nextInt(10);

        for (int i = 0; i < chestSize; i++) {
            contents.add(items.next());
        }

        Collections.shuffle(new ArrayList<>(contents));

        for (MyItem item : contents) {
            if (!inventory.contains(item.getItem())) {
                inventory.setItem(random.nextInt(inventory.getSize()), item.getItem());
            }
        }
    }

    public enum ChestType {

        ISLAND, MIDDLE
    }
}
