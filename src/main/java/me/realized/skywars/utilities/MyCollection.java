package me.realized.skywars.utilities;

import java.util.*;

class MyCollection<E> {

    private final NavigableMap<Double, E> map = new TreeMap<>();
    private final Random random = new Random();
    private double total = 0;

    public void add(double weight, E result) {
        if (weight <= 0) {
            return;
        }

        total += weight;
        map.put(total, result);
    }

    public E next() {
        double value = random.nextDouble() * total;
        return map.ceilingEntry(value).getValue();
    }

    public Collection<E> getContents() {
        return map.values();
    }
}
