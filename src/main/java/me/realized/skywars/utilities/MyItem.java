package me.realized.skywars.utilities;

import org.bukkit.inventory.ItemStack;

public class MyItem {

    private final double weight;
    private final ItemStack base;

    public MyItem(double weight, ItemStack base) {
        this.weight = weight;
        this.base = base;
    }

    public double getWeight() {
        return weight;
    }

    public ItemStack getItem() {
        return base;
    }
}
