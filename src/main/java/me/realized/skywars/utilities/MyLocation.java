package me.realized.skywars.utilities;

import org.bukkit.Location;

import java.io.Serializable;

public class MyLocation implements Serializable {

    private static final long serialVersionUID = 9190568900034354811L;

    private final int x;
    private final int y;
    private final int z;

    public MyLocation(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public MyLocation(Location location) {
        this.x = location.getBlockX();
        this.y = location.getBlockY();
        this.z = location.getBlockZ();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public double distance(MyLocation other) {
        double x = (this.x - other.getX()) * (this.x - other.getX());
        double y = (this.y - other.getY()) * (this.y - other.getY());
        double z = (this.z - other.getZ()) * (this.z - other.getZ());
        return Math.abs(Math.sqrt(x + y + z));
    }
}
