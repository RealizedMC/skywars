package me.realized.skywars.utilities;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class NMSUtil {

    public enum TitleAction {
        DISPLAY, CLEAR, RESET
    }

    public static void title(Player player, String title, String subtitle, int fadeIn, int stay, int fadeOut, TitleAction action) {
        Class<?> packetTitle = getNMSClass("PacketPlayOutTitle");
        Class<?> actions = getNMSClass("PacketPlayOutTitle$EnumTitleAction");
        Class<?> chatBaseComponent = getNMSClass("IChatBaseComponent");
        Class<?> chatSerializer = getNMSClass("IChatBaseComponent$ChatSerializer");

        if (packetTitle == null || actions == null || chatBaseComponent == null || chatSerializer == null) {
            return;
        }

        Object[] enumActions = actions.getEnumConstants();
        Object packet;

        try {
            switch (action) {
                case DISPLAY:
                    packet = packetTitle.getConstructor(actions, chatBaseComponent, int.class, int.class, int.class).newInstance(enumActions[2], null, fadeIn, stay, fadeOut);
                    sendPacket(player, packet);

                    packet = packetTitle.getConstructor(actions, chatBaseComponent).newInstance(enumActions[0], chatSerializer.getMethod("a", String.class).invoke(null, "{\"text\": \"" + title + "\"}"));
                    sendPacket(player, packet);

                    if (subtitle != null && !subtitle.equals("")) {
                        packet = packetTitle.getConstructor(actions, chatBaseComponent).newInstance(enumActions[1], chatSerializer.getMethod("a", String.class).invoke(null, "{\"text\": \"" + subtitle + "\"}"));
                        sendPacket(player, packet);
                    }

                    break;
                case CLEAR:
                    packet = packetTitle.getConstructor(actions, chatBaseComponent).newInstance(enumActions[2], null);
                    sendPacket(player, packet);
                    break;
                case RESET:
                    packet = packetTitle.getConstructor(actions, chatBaseComponent).newInstance(enumActions[3], null);
                    sendPacket(player, packet);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void sendPacket(Player player, Object packet) {
        try {
            Object handle = player.getClass().getMethod("getHandle").invoke(player);
            Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
            playerConnection.getClass().getMethod("sendPacket", getNMSClass("Packet")).invoke(playerConnection, packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Class<?> getNMSClass(String name) {
        try {
            return Class.forName("net.minecraft.server." + Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3] + "." + name);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }
}
